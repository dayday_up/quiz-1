package com.twuc.webApp.controller;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.springframework.http.MediaType.APPLICATION_JSON;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@SpringBootTest
@AutoConfigureMockMvc
class GameControllerTest {
    @Autowired
    private MockMvc mockMvc;

    private String createGameAndReturnLocation() throws Exception {
        ResultActions actions = mockMvc.perform(post("/api/games"));
        return actions.andReturn().getResponse().getHeader("Location");
    }

    @Test
    void should_return_201_when_create_game() throws Exception {
        ResultActions actions = mockMvc.perform(post("/api/games"));
        actions.andExpect(status().isCreated())
                .andExpect(header().exists("Location"));

    }

    @Test
    void should_return_200_when_guess_answer() throws Exception {

        String url = createGameAndReturnLocation();
        mockMvc.perform(patch(url)
                .contentType(APPLICATION_JSON)
                .content("{\"answer\": \"1234\" }"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.hint").exists())
                .andExpect(jsonPath("$.correct").value(false));
    }

    @Test
    void should_create_several_games_at_same_time() throws Exception {
        String firstGameUrl = createGameAndReturnLocation();
        String secondGameUrl = createGameAndReturnLocation();
        String thirdGameUrl = createGameAndReturnLocation();

        long count = Stream.of(firstGameUrl, secondGameUrl, thirdGameUrl).distinct().count();
        assertEquals(3, count);
    }

    @Test
    void should_return_status_of_game() throws Exception {
        String url = createGameAndReturnLocation();
        mockMvc.perform(get(url))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.answer").exists())
                .andExpect(jsonPath("$.status").exists());
    }

    @Test
    void should_return_404_when_gameId_is_not_exist() throws Exception {
        // TODO 如何确保gameId不存在
        mockMvc.perform(get("/api/games/1234"))
                .andExpect(status().isNotFound());
    }

    @Test
    void should_return_400_when_gameId_is_not_valid() throws Exception {
        should_return_400("12345");
        should_return_400("a4cd");
        should_return_400("1122");
    }
    private <T> void should_return_400(T answer) throws Exception {
        String url = createGameAndReturnLocation();
        ResultActions resultActions = mockMvc.perform(patch(url)
        .content("{\"answer\": \"" + answer + "\" }")
        .contentType(APPLICATION_JSON))
        .andExpect(status().isBadRequest());
    }
}