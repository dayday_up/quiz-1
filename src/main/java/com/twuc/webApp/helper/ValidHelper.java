package com.twuc.webApp.helper;

import java.util.regex.Pattern;

public class ValidHelper {
    public static Boolean validAnswer(String answer) {
        if (!isValidNumber(answer) || hasRepeatChar(answer)) {
            throw new IllegalArgumentException();
        }
        return false;
    }

    private static Boolean hasRepeatChar(String str) {
        char[] elements = str.toCharArray();
        for (char e: elements){
            if (str.indexOf(e) != str.lastIndexOf(e)) {
                return true;
            }
        }
        return false;
    }

    private static Boolean isValidNumber(String str) {
        return str.matches("\\d{4}");
    }
}
