package com.twuc.webApp.model;


public class Game {
    private Integer id;
    private Boolean status;
    // TODO 修改string为Integer类型
    private String answer;

    public Game(Integer id) {
        this.id = id;
        this.status = false;
        this.answer = generateRandomAnswer();
    }

    private String generateRandomAnswer() {
        //TODO 验重 SET
        return String.valueOf((int) ((Math.random() * 9 + 1) * 1000));
    }

    public Integer getId() {
        return id;
    }

    public Boolean getStatus() {
        return status;
    }

    public String getAnswer() {
        return answer;
    }
}
