package com.twuc.webApp.model;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Answer {
    @JsonProperty("answer")
    private String value;

    public void setValue(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }
}
