package com.twuc.webApp.model;

public class Hint {
    private String hint;
    private Boolean correct;

    public Hint() {
    }

    public Hint(String hint, Boolean correct) {
        this.hint = hint;
        this.correct = correct;
    }

    public Hint createHint(Game game, String userAnswer) {
        StringBuilder builder = new StringBuilder();
        int correctPosition = 0;
        int correctValue = 0;
        if (userAnswer.equals(game.getAnswer())) {
            builder.append("4A0B");
        } else {
            for (int i = 0; i < game.getAnswer().length(); i++) {
                char answerChar = game.getAnswer().charAt(i);
                if (answerChar == userAnswer.charAt(i)) {
                    correctPosition += 1;
                }
                for (int j = 0; j < userAnswer.length(); j++) {
                    char userAnswerChar = userAnswer.charAt(j);
                    if (answerChar == userAnswerChar) {
                        correctValue += 1;
                    }
                }
            }
            builder.append(correctPosition)
                    .append("A")
                    .append(correctValue)
                    .append("B");
        }

        return new Hint(builder.toString(), game.getStatus());
    }

    public String getHint() {
        return hint;
    }

    public void setHint(String hint) {
        this.hint = hint;
    }

    public Boolean getCorrect() {
        return correct;
    }

    public void setCorrect(Boolean correct) {
        this.correct = correct;
    }
}
