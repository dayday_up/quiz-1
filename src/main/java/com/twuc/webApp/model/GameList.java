package com.twuc.webApp.model;

import java.util.ArrayList;
import java.util.List;

public class GameList {

    private List<Game> games = new ArrayList<Game>();

    public void add(Game game) {
        this.games.add(game);
    }

    public List<Game> getGames() {
        return games;
    }
}
