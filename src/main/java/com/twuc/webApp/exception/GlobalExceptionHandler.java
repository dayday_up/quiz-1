package com.twuc.webApp.exception;

import org.springframework.core.NestedCheckedException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
public class GlobalExceptionHandler {
    @ExceptionHandler(IndexOutOfBoundsException.class)
    public ResponseEntity<String> IndexOutOfBoundsException(Exception exception) {
        return ResponseEntity.status(HttpStatus.NOT_FOUND)
                .body("Error");
    }

    @ExceptionHandler(IllegalArgumentException.class)
    public ResponseEntity<String> handleIllegalArgumentsException(Exception exception) {
        return ResponseEntity.status(HttpStatus.BAD_REQUEST)
                .body("Error");
    }
}
