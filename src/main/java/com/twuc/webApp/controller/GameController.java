package com.twuc.webApp.controller;

import com.twuc.webApp.helper.ValidHelper;
import com.twuc.webApp.model.Answer;
import com.twuc.webApp.model.Game;
import com.twuc.webApp.model.GameList;
import com.twuc.webApp.model.Hint;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/api")
public class GameController {
    private List<Game> gameList = new ArrayList<>();

    @PostMapping("/games")
    @ResponseStatus(HttpStatus.CREATED)
    public ResponseEntity createGame() {
        int gameId = gameList.size();
        gameList.add(new Game(gameId));
        return ResponseEntity.status(HttpStatus.CREATED)
                .header("Location", "/api/games/" + gameId)
                .build();
    }

    @PatchMapping("/games/{gameId}")
    public ResponseEntity playGame(@PathVariable Integer gameId, @RequestBody Answer answer) {
        Game currentGame = gameList.get(gameId);
        ValidHelper.validAnswer(answer.getValue());
        Hint hint = new Hint().createHint(currentGame, answer.getValue());
        return ResponseEntity.status(HttpStatus.OK)
                .body(hint);
    }

    @GetMapping("/games/{gameId}")
    public ResponseEntity getGameStatus(@PathVariable Integer gameId) {
        Game currentGame = gameList.get(gameId);
        return ResponseEntity.status(HttpStatus.OK)
                .body(currentGame);
    }
}
